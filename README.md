GoogleTest - Docker container
=============================

## Summary

- [X] What is? What's the purpose?
- [X] Repository structure
- [X] How to build locally
- [ ] How to use it for your CI/CD
- [ ] How the CI/CD works
- [ ] Adding a new architecture to CI/CD

## What is? What's the purpose?

This repository hosts a docker container image. It includes the Dockerfile script for assembling
the image on the go, as well as additional files to make it easy to build and test locally and
for CI/CD pipeline process.

The docker image is based on the Ubuntu docker image and contains the following additional
packages/tools:
- **cmake**
- **gcc/g++**
- **GoogleTest suite (gtest/gmock)**

The main purpose of this repository is to automatically build the image for the configured
architectures and host them with the provided GitLab Container Registry facility.

Access the built images on the project's container registry link:
https://gitlab.com/natanaeljr/docker-gtest/container_registry

And pull the _latest_ built image with: (You must login with a gitlab account)
```
docker login registry.gitlab.com
docker pull registry.gitlab.com/natanaeljr/docker-gtest/ubuntu-gtest
```

## Repository structure

    ├── Dockerfile          Docker image build script.
    ├── Makefile            Defines targets to build and test the docker image.
    ├── test                Contains a C++ project with GTest/GMock testing
    │   ├── ...               for running on the container.
    │   └── run_test.sh     Minor script for build and running the test project.
    ├── .gitignore          Gitignore file.
    ├── .gitlab-ci.yml      GitLab CI/CD configuration YAML.
    └── README.md           This README file.

## How to build locally

You may want to build/test the image locally by using the Make targets defined in the Makefile.

1. Clone the repository and `cd` it.
2. Run `make build` to build
3. Run `make test` to test ;)

You can pass the following options to ``make``:
- `TARGET_ARCH`: Target architecture. e.g: x86_64
- `CONTAINER_TAG`: Cointainer tag name. e.g: latest
- `CONTAINER_IMAGE`: Container image name. e.g: namespace/gtest

Example:
```
make build CONTAINER_IMAGE=natanaeljr/gtest TARGET_ARCH=armhf CONTAINER_TAG=dev
make test CONTAINER_IMAGE=natanaeljr/gtest TARGET_ARCH=armhf CONTAINER_TAG=dev
